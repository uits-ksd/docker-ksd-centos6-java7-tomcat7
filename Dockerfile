# Tomcat7

# Pull base image.
FROM uaecs/docker-eas-centos6-java7

# Install updates
RUN \
  yum update -y && \
  rm -rf /var/lib/apt/lists/*

# Manually install Tomcat
ENV CATALINA_HOME /opt/tomcat7

RUN wget http://mirror.symnds.com/software/Apache/tomcat/tomcat-7/v7.0.75/bin/apache-tomcat-7.0.75.tar.gz && \
        tar -xvf apache-tomcat-7.0.75.tar.gz && \
        rm apache-tomcat-7.0.75.tar.gz && \
        mv apache-tomcat* ${CATALINA_HOME}

RUN chmod +x ${CATALINA_HOME}/bin/*sh

# Create links that KFS image expects (TODO should be vice-versa; update KFS image to be CentOS'y)
RUN ln -s ${CATALINA_HOME} /var/lib/tomcat7
  
EXPOSE 8080

